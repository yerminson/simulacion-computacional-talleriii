#include "montecarlo.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

using std::vector;
using std::cout;
using std::cin;
using std::endl;

MonteCarlo::MonteCarlo()
{
    srand(time(NULL));
}

 double MonteCarlo::areaCirculoN(int dimension,int hits)
 {
    vector<double> coordenadas;
    int cantidadPuntos = 0;
    for(int j=0;j<hits;j++)
    {
        for(int i=0;i<dimension;i++)
        {
            double alea = (double)rand()/RAND_MAX;
            coordenadas.push_back(alea);
        }
        if(perteneceCirculoN(coordenadas))
        {
            cantidadPuntos++;
        }
        coordenadas.clear();
    }

    //cout << "cantidad puntos: " << cantidadPuntos << endl;
    double area = cantidadPuntos/(double)hits;
    area *= pow(2,dimension);
    return area;
 }

 bool MonteCarlo::perteneceCirculoN(vector<double> coordenadas)
 {
     double v = 0.0;
     for(int i=0;i<coordenadas.size();i++)
     {
         v += pow(coordenadas.at(i),2.0);
     }
     if(v <= 1.0)
     {
         return true;
     }else
     {
         return false;
     }
 }

 double MonteCarlo::areaPoligono(std::vector<Punto> vertices, int hits)
 {
     int cantidadPuntos = 0;
     Punto pm = puntoMax(vertices);
     for(int j=0;j<hits;j++)
     {
         double x = (double)rand()/RAND_MAX;
         double y = (double)rand()/RAND_MAX;
         x *= pm.getX();
         y *= pm.getY();
         Punto p(x,y);

         if(pertenecePuntoPoligono(vertices,p))
         {
             cantidadPuntos++;
         }
     }

     //cout << "cantidad puntos: " << cantidadPuntos << endl;
     double area = (cantidadPuntos/(double)hits)*(pm.getX()*pm.getY());
     return area;
 }

 bool MonteCarlo::pertenecePuntoPoligono(std::vector<Punto> vertices, Punto punto)
 {
    int contador = 0;
    double xinters;
    Punto p1,p2;
    int n = vertices.size();

    p1 = vertices.at(0);
    for (int i=1;i<=n;i++)
    {
        p2 = vertices[i % n];
        if (punto.getY() > min(p1.getY(),p2.getY()))
        {
            if (punto.getY() <= max(p1.getY(),p2.getY()))
            {
                if (punto.getX() <= max(p1.getX(),p2.getX()))
                {
                    if (p1.getY() != p2.getY())
                    {
                        xinters = (punto.getY()-p1.getY())*(p2.getX()-p1.getX())/(p2.getY()-p1.getY())+p1.getX();
                        if (p1.getX() == p2.getX() || punto.getX() <= xinters)
                        {
                            contador++;
                        }
                    }
                }
            }
        }
        p1 = p2;
    }

    if (contador%2 == 0)
        return(false);
    else
        return(true);
 }

 double MonteCarlo::min(double x, double y)
 {
     if(x < y)
         return x;
     else
         return y;

 }

 double MonteCarlo::max(double x, double y)
 {
     if(x > y)
         return x;
     else
         return y;
 }

 Punto MonteCarlo::puntoMax(std::vector<Punto> vertices)
 {
    double xMax = 0;
    double yMax = 0;
    for(int i=0;i<vertices.size();i++)
    {
        Punto p = vertices.at(i);
        if(p.getX() > xMax)
        {
            xMax = p.getX();
        }
        if(p.getY() > yMax)
        {
            yMax = p.getY();
        }
    }
    return Punto(xMax,yMax);
 }


  int MonteCarlo::busquedaBinaria(double valorReal, int dimension)
  {
      int lo =0;
      int hi = 5000000;
      int mid = 0;
      double valorCalculado = 0;
      double eps = 0.001;    
      double valor = 0 ;      

      while (lo < hi){
		  
          mid = lo + (hi-lo)/2;
		  valorCalculado = areaCirculoN(dimension,mid);
          valor  = fabs((valorReal - valorCalculado)) ;

          if(valor < eps){
            hi = mid;
          }else{
            lo = mid+1;
          }
      }

        cout << "Valor Real " << valorReal << "\n";
        cout << "Valor Encontrado "<<valorCalculado << "\n";
        cout << "Error "<< valor << "\n \n";
		
		
        return lo;
  }

vector<int> MonteCarlo::busquedaHitsParaAreaDada(std::vector<double> valorReales){


    vector<int> hitsEncontrados;
    int contador = 0;
    int acumulado = 0;
    int casosPrueba =  10;
    
    

    for(int i=0; i<valorReales.size();i++){

        cout  <<"Dimension : " << (i+2)<<"\n";
            for(int j=0; j< casosPrueba; j++){
                int dimension = i+2;
                double valorReal = valorReales[i];

                acumulado += busquedaBinaria(valorReal,dimension);

                contador++;
            }
		
            hitsEncontrados.push_back(acumulado/casosPrueba);
            cout  <<"Hits Necesarios : " <<hitsEncontrados.at(i)<<"\n" << "\n";
            acumulado = 0;
    }

    return hitsEncontrados;
}
