#include "punto.h"

Punto::Punto()
{
    this->x = 0;
    this->y = 0;
}

Punto::Punto(double x, double y)
{
    this->x = x;
    this->y = y;
}

double Punto::getX()
{
    return x;
}

double Punto::getY()
{
    return y;
}

void Punto::setX(double x)
{
    this->x = x;
}

void Punto::setY(double y)
{
    this->y = y;
}

void Punto::operator=(Punto otroPunto)
{
    this->x = otroPunto.getX();
    this->y = otroPunto.getY();
}
